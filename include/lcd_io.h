/** @file lcd_io.h
 *
 * LCD IO library - main set of helper functions
 *
 * This file is part of LCD IO library for ST7066U display module
 * used in combination with custom AT90USB647 development board created by
 * University of Tartu and used in its "Microprocessors" course.
 *
 * This file contains constants and declarations of helper functions that allow
 * convenient use of LCD module.
 *
 * Note that if you need only LCD raw interface (writing binary instructions
 * and data directly to LCD RAM, etc), you should use **lcd_io_core** instead.
 * **Do not use functions from both lcd_io and lcd_io_core together!**
 *
 * ### Usage
 *
 * Display MCU must be initialized before any other instruction can be used.
 * To do this, simply call lcdInit() function.
 *
 * #### Example
 *
 * @code
 * lcdInit();
 * // now other functions can be used
 * lcdWriteSz("Hello World!");
 * @endcode
 *
 * ### Additional notes
 *
 * Functions in this library ignore LCD MCU delay requirements
 * for the sake of performance and in good faith that **in most cases**
 * on frequencies at or below 16MHz instructions will work without them
 * anyway.
 *
 * This library directly supports only left-to-right data output!
 *
 * @version 1.0
 * @date 03.06.2015
 * @author Sergei Jakovlev
 * @copyright The MIT License (MIT)
 */


#ifndef _LCD_IO_H_
#define _LCD_IO_H_


#include <avr/io.h>


/**
 * Initialize LCD screen (both physical pins and LCD MCU initialization)
 */
void lcdInit();


/**
 * Move cursor home (to first symbol of first line)
 */
void lcdCursorReturnHome();


/**
 * Move cursor to specified position without modifying displayed data
 *
 * @param line 0 for the first line, 1 for the second line
 * @param position cursor position, must be in range from 0x00 to 0x27 (incl).
 */
void lcdCursorToPosition(uint8_t line, uint8_t position);


/**
 * Move cursor to first symbol of first line
 */
void lcdLine1();


/**
 * Move cursor to first symbol of second line
 */
void lcdLine2();


/**
 * Shift cursor one place to the right
 */
void lcdShiftCursorRight();


/**
 * Shift cursor one place to the left
 */
void lcdShiftCursorLeft();


/**
 * Shift display one place to the right
 */
void lcdShiftDisplayRight();


/**
 * Shift display one place to the left
 */
void lcdShiftDisplayLeft();


/**
 * Display char at current cursor position
 *
 * This function moves cursor to next position (left-to-right).
 *
 * @param c char that will be added to corresponding line on LCD
 */
void lcdWriteChar(const char c);


/**
 * Display zero terminated string beginning at current cursor position
 *
 * This function moves cursor by length of provided string to the right.
 *
 * @param szIn zero terminated string
 */
void lcdWriteSz(const char szIn[]);


/**
 * Save custom symbol in CGRAM
 *
 * Calling this function overwrites previously saved symbol with the same code.
 *
 * @param symCode code used to display this symbol (has to be written
 * to DDRAM to display this character). **Must be in range from 0x00 to 0x07.**
 *
 * @param symData custom character 5x8 bitmap, given as an uint8_t array.
 * Array must contain 8 values. Each uint8_t value represents one row of
 * custom symbol (from top to bottom). Three high bits are not used and can be
 * set to anything. Five low bits are mapped to symbol bitmap row from left
 * to right, with 1 representing enabled and 0 disabled pixel.
 */
void lcdSaveSymbol(const uint8_t symCode, const uint8_t symData[]);


/**
 * Turn cursor on
 *
 * Cursor is displayed as a solid line in last row of symbol bitmap.
 *
 * @see lcdBlinkerOn() - enables blinking effect at cursor position
 */
void lcdCursorOn();


/**
 * Turn cursor off
 *
 * @see lcdBlinkerOff() - disables blinking effect at cursor position
 */
void lcdCursorOff();


/**
 * Turn blinker on
 *
 * Note blinker is "blinking" effect displayed at cursor position that is
 * independent from cursor visibility.
 *
 * @see lcdCursorOn() - enables cursor display
 */
void lcdBlinkerOn();


/**
 * Turn blinker off
 *
 * Note blinker is "blinking" effect displayed at cursor position that is
 * independent from cursor visibility.
 *
 * @see lcdCursorOff() - disables cursor display
 */
void lcdBlinkerOff();


/**
 * Turn display on
 */
void lcdDisplayOn();


/**
 * Turn display off
 */
void lcdDisplayOff();


/**
 * Clear display and move cursor to beginning of the first row
 */
void lcdClear();


#endif /* _LCD_IO_H_ */
