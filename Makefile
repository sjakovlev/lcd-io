###########################
# LCD IO makefile         #
#                         #
# Author: Sergei Jakovlev #
# Date:   03.06.2015      #
###########################


###########
# Options #
###########

CC := avr-gcc
CFLAGS := -Os -std=gnu11 -DF_CPU=2000000UL -ffunction-sections -fdata-sections -Wall -Wextra -Wpedantic

LD := avr-gcc
LDFLAGS := -std=gnu11 -Wl,--gc-sections -Wall -Wextra -Wpedantic

SRC_DIR    := src
SAMPLE_DIR := samples
HEADER_DIR := include
BUILD_DIR  := build
DOCS_DIR   := docs

MCU_TARGET := at90usb647
AVRDUDE_TARGET := usb647



#########################
# Library build targets #
#########################

all: $(BUILD_DIR)/lcd_io_core.o $(BUILD_DIR)/lcd_io.o

$(BUILD_DIR)/lcd_io_core.o: $(SRC_DIR)/lcd_io_core.c | $(BUILD_DIR)
	$(CC) $(CFLAGS) -c -mmcu=$(MCU_TARGET) -I$(HEADER_DIR) $< -o $@

$(BUILD_DIR)/lcd_io.o: $(SRC_DIR)/lcd_io.c | $(BUILD_DIR)
	$(CC) $(CFLAGS) -c -mmcu=$(MCU_TARGET) -I$(HEADER_DIR) $< -o $@



########################
# Sample build targets #
########################

$(BUILD_DIR)/lcd_tester.o: $(SAMPLE_DIR)/lcd_tester.c | $(BUILD_DIR)
	$(CC) $(CFLAGS) -c -mmcu=$(MCU_TARGET) -I$(HEADER_DIR) $< -o $@

$(BUILD_DIR)/lcd_tester_linked.o: $(BUILD_DIR)/lcd_tester.o $(BUILD_DIR)/lcd_io.o $(BUILD_DIR)/lcd_io_core.o
	$(LD) $(LDFLAGS) -mmcu=$(MCU_TARGET) -I$(HEADER_DIR) $^ -o $@

$(BUILD_DIR)/lcd_tester_linked.hex: $(BUILD_DIR)/lcd_tester_linked.o
	avr-objcopy -j .text -j .data -O ihex $< $@

flash_lcd_tester: $(BUILD_DIR)/lcd_tester_linked.hex
	avrdude -c flip1 -p $(AVRDUDE_TARGET) -U flash:w:$<



#####################
# Auxillary targets #
#####################

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

docs:
	doxygen

clean:
	rm -f $(BUILD_DIR)/*.o $(BUILD_DIR)/*.hex
	rm -rf $(DOCS_DIR)



.PHONY: all clean docs flash_lcd_tester
