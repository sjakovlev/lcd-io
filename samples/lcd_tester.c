/** @file lcd_tester.c
 *
 * Sample program written using LCD IO library.
 *
 * This program demonstrates some basic functions of LCD IO library.
 *
 * @version 1.0
 * @date 03.06.2015
 * @author Sergei Jakovlev
 * @copyright The MIT License (MIT)
 */


#include <avr/io.h>
#include <util/delay.h>
#include "lcd_io.h"


/**
 * Simple delay function (for delays in range from 0 to 255 seconds)
 *
 * @param sec delay in seconds
 */
void delaySec(uint8_t sec) {
	for (uint8_t i = 0; i < sec; i++) {
		_delay_ms(250);
		_delay_ms(250);
		_delay_ms(250);
		_delay_ms(250);
	}
}


/**
 * Initialize and launch project
 *
 * @return nothing, ends with endless loop
 */
int main() {
	lcdInit();
	lcdCursorOff();
	lcdBlinkerOff();

	while (1) {
		lcdClear();
		lcdWriteSz("LCD TESTER  v1.0");

		delaySec(2);

		lcdClear();
		lcdLine2();
		lcdWriteSz("Cursor on");
		lcdLine1();
		lcdCursorOn();

		delaySec(2);

		lcdCursorOff();
		lcdClear();
		lcdLine2();
		lcdWriteSz("Blinker on");
		lcdLine1();
		lcdBlinkerOn();

		delaySec(2);

		lcdClear();
		lcdLine2();
		lcdWriteSz("Both on");
		lcdLine1();
		lcdCursorOn();

		delaySec(2);

		lcdClear();
		lcdLine2();
		lcdWriteSz("Cursor shift");
		lcdLine1();
		delaySec(1);
		lcdShiftCursorRight();
		delaySec(1);
		lcdShiftCursorRight();
		delaySec(1);
		lcdWriteSz("OK");
		delaySec(1);
		lcdShiftCursorLeft();
		delaySec(1);
		lcdShiftCursorLeft();

		delaySec(2);

		lcdClear();
		lcdLine2();
		lcdWriteSz("Display shift");
		lcdLine1();
		delaySec(1);
		lcdShiftDisplayRight();
		delaySec(1);
		lcdShiftDisplayRight();
		delaySec(1);
		lcdWriteSz("OK");
		delaySec(1);
		lcdShiftDisplayLeft();
		delaySec(1);
		lcdShiftDisplayLeft();

		delaySec(2);

		lcdClear();
		lcdCursorToPosition(1, 4);
		lcdWriteSz("Random");
		lcdCursorToPosition(0, 8);
		lcdWriteSz("position");

		delaySec(2);

		lcdBlinkerOff();
		lcdCursorOff();
		lcdClear();
		lcdCursorReturnHome();
		lcdWriteSz("LCD turning OFF");
		lcdLine2();
		lcdWriteSz("in 2 seconds");

		delaySec(2);

		lcdDisplayOff();

		delaySec(2);

		lcdDisplayOn();
	}
}
