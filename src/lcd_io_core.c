/** @file lcd_io_core.c
 *
 * LCD IO library - core definitions and functions (implementations)
 *
 * This file is part of LCD IO library for ST7066U display module
 * used in combination with custom AT90USB647 development board created by
 * University of Tartu and used in its "Microprocessors" course.
 *
 * This file contains implementations of core helper functions provided by
 * the LCD IO library.
 *
 * Note: this implementation assumes that MCU maximal frequency is 16MHz.
 * In that case, each cycle takes no less than 62.5ns. This is used for
 * extremely short delays required by display module (implemented by NOPs).
 * See #LCD_E and display documentation for details on required timings.
 *
 * @version 1.0
 * @date 03.06.2015
 * @author Sergei Jakovlev
 * @copyright The MIT License (MIT)
 */


#include <avr/io.h>
#include <util/delay.h> /* use provided delay functions */
#include "lcd_io_core.h"


inline void lcdInitPorts() {
	// DB as input by default
	// [not needed since all other instructions change this state]
	//LCD_DB_DDR = 0;

	// RS and RW as output
	LCD_RS_RW_DDR |= LCD_RS_MASK | LCD_RW_MASK;

	// E as output
	LCD_E_DDR |= LCD_E_MASK;
}


void lcdInitInstr() {
	// Note that delays are required by LCD MCU to successfully execute
	// given instructions. For precise delay descriptions, see LCD_xxx_INSTR
	// documentation.

	// Set display function: use 8-bit data bus and 2 lines
	lcdWriteInstrNoCheckRev(LCD_FUNCTION_SET_INSTR_REV |
			LCD_FUNCTION_SET_8BIT_REV | LCD_FUNCTION_SET_2LINE_REV);
	_delay_us(37);

	// Previous instruction needs to be sent twice (by default LCD MCU may
	// use 4-bit data bus, which requires this instruction to be sent 2 times)
	lcdWriteInstrNoCheckRev(LCD_FUNCTION_SET_INSTR_REV |
			LCD_FUNCTION_SET_8BIT_REV | LCD_FUNCTION_SET_2LINE_REV);
	_delay_us(37);

	// Display on, cursor and blinker on
	lcdWriteInstrRev(LCD_ON_OFF_INSTR_REV | LCD_DISPLAY_ON_REV |
			LCD_CURSOR_ON_REV | LCD_BLINKER_ON_REV);
	_delay_us(37);

	// Clear display
	lcdWriteInstrRev(LCD_CLEAR_DISPLAY_REV);
	_delay_ms(1.52);

	// Entry mode: move cursor right
	lcdWriteInstrRev(LCD_ENTRY_MODE_SET_INSTR_REV |
			LCD_ENTRY_MODE_SET_SHIFT_RIGHT_REV);
	_delay_us(37);
}


void lcdInitInstrWithDelay() {
	_delay_ms(40);
	lcdInitInstr();
}


void lcdWaitWhileBusy() {
	LCD_DB_DDR = 0x00;	// DB as input
	LCD_DB_PORT = 0xff;	// Pull-up on DB port

	LCD_RS_RW_PORT &= ~LCD_RS_MASK;		// RS=0 (instruction)
	LCD_RS_RW_PORT |= LCD_RW_MASK;		// RW=1 (read)

	uint8_t state;

	// Check until busy flag is removed

	do {
		LCD_E_PORT |= LCD_E_MASK;		// E=1

		asm volatile ("nop");			// wait at least 100ns
		asm volatile ("nop");			// (data setup time)

		state = LCD_DB_PIN;				// read state
		LCD_E_PORT &= ~LCD_E_MASK;		// E=0

		_delay_us(1);					// wait 1000ns (for minimal cycle time
										// of 1200ns)

	} while (state & LCD_BUSY_MASK_REV);
}


void lcdWriteInstrNoCheckRev(const uint8_t instr) {
	LCD_DB_DDR = 0xFF;								// DB as output

	LCD_RS_RW_PORT &= ~(LCD_RS_MASK | LCD_RW_MASK); // RS=0, RW=0
	LCD_E_PORT |= LCD_E_MASK; 						// E=1

	LCD_DB_PORT = instr;							// write instruction

	asm volatile ("nop");							// wait at least 140-60=80ns
	asm volatile ("nop");							// (need 140 pulse width)

	LCD_E_PORT &= ~LCD_E_MASK;						// E=0
	//asm volatile ("nop");							// wait at least 10ns
}


void lcdWriteInstrRev(const uint8_t instr) {
	lcdWaitWhileBusy();
	lcdWriteInstrNoCheckRev(instr);
}


void lcdWriteDataRev(const uint8_t data) {
	lcdWaitWhileBusy();
	LCD_DB_DDR = 0xFF;	// DB as output

	LCD_RS_RW_PORT |= LCD_RS_MASK;	// RS=1 (data)
	LCD_RS_RW_PORT &= ~LCD_RW_MASK; // RW=0 (write)
	LCD_E_PORT |= LCD_E_MASK; 		// E=1
	LCD_DB_PORT = data;				// write data

	asm volatile ("nop");			// wait at least 140-60=80ns
	asm volatile ("nop");			// (need 140 pulse width)

	LCD_E_PORT &= ~LCD_E_MASK;		// E=0
	//asm volatile ("nop");			// wait at least 10ns
}


void lcdWriteData(uint8_t data) {
	lcdWriteDataRev(lcdReverseBits(data));
}


uint8_t lcdReadDataRev() {
	lcdWaitWhileBusy();

	// DB already set as input with pull-up after lcdWaitWhileBusy();
	//LCD_DB_DDR = 0x00;	// DB as input
	//LCD_DB_PORT = 0xff;	// Pull-up on DB port

	LCD_RS_RW_PORT |= LCD_RS_MASK | LCD_RW_MASK;	// RS=1 (data), RW=1 (read)

	uint8_t data;

	LCD_E_PORT |= LCD_E_MASK;		// E=1

	asm volatile ("nop");			// wait at least 100ns
	asm volatile ("nop");			// (data setup time)

	data = LCD_DB_PIN;				// read data
	LCD_E_PORT &= ~LCD_E_MASK;		// E=0

	return data;
}


uint8_t lcdReadData() {
	return lcdReverseBits(lcdReadDataRev());
}


uint8_t lcdReverseBits(uint8_t data) {
	data = (data & 0xF0) >> 4 | (data & 0x0F) << 4;
	data = (data & 0xCC) >> 2 | (data & 0x33) << 2;
	data = (data & 0xAA) >> 1 | (data & 0x55) << 1;
	return data;
}
