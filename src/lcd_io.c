/** @file lcd_io.c
 *
 * LCD IO library - main set of helper functions (implementations)
 *
 * This file is part of LCD IO library for ST7066U display module
 * used in combination with custom AT90USB647 development board created by
 * University of Tartu and used in its "Microprocessors" course.
 *
 * This file contains implementations of helper functions that allow
 * convenient use of LCD module.
 *
 * Note that if you need only LCD raw interface (writing binary instructions
 * and data directly to LCD RAM, etc), you should use **lcd_io_core** instead.
 * **Do not use functions from both lcd_io and lcd_io_core together!**
 *
 * **Warning:** functions in this library ignore LCD MCU delay requirements
 * for the sake of performance and in good faith that **in most cases**
 * on frequencies at or below 16MHz instructions will work without them
 * anyway.
 *
 * @version 1.0
 * @date 03.06.2015
 * @author Sergei Jakovlev
 * @copyright The MIT License (MIT)
 */


#include <avr/io.h>
#include "lcd_io_core.h"
#include "lcd_io.h"


/**
 * Current state of LCD
 *
 * This variable stores blinker, cursor and display state. It can be used
 * directly when calling lcdWriteInstrRev() to restore last stored state of LCD.
 */
static uint8_t lcdState = 0;


/**
 * Current position of LCD cursor
 *
 * This variable is used to restore correct cursor position after CGRAM
 * operations.
 */
static uint8_t lcdCursorPosition = 0;


void lcdInit() {
	lcdInitPorts();
	lcdInitInstrWithDelay();

	lcdState = LCD_ON_OFF_INSTR_REV | LCD_DISPLAY_ON_REV | LCD_CURSOR_ON_REV |
			LCD_BLINKER_ON_REV;
}


void lcdWriteChar(const char c) {
	lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR_REV |
			lcdReverseBits(lcdCursorPosition));
	lcdWriteData(c);
	lcdCursorPosition++;
}


/**
 * Check if given position is within area available to store chars to DDRAM
 *
 * @param position cursor position
 * @return 1 if char can be stored at cursor, 0 otherwise
 */
static uint8_t lcdIsCursorPositionInRange(uint8_t position) {
	if ((position >= LCD_FIRST_LINE_START_ADDR) &&
			(position < LCD_FIRST_LINE_END_ADDR)) {
		return 1;
	}

	if ((position >= LCD_SECOND_LINE_START_ADDR) &&
			(position < LCD_SECOND_LINE_END_ADDR)) {
		return 1;
	}

	return 0;
}


void lcdWriteSz(const char szIn[]) {
	lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR_REV |
			lcdReverseBits(lcdCursorPosition));

	uint8_t i = 0;
	uint8_t curChar = szIn[0];

	while (curChar != '\0' && lcdIsCursorPositionInRange(lcdCursorPosition)) {
		lcdWriteData(curChar);
		curChar = szIn[++i];
		lcdCursorPosition++;
	};
}


void lcdCursorReturnHome() {
	lcdWriteInstrRev(LCD_RETURN_HOME_REV);
	lcdCursorPosition = 0;
}


void lcdCursorToPosition(uint8_t line, uint8_t position) {
	if (position >= LCD_TWO_LINE_LENGTH || line >= 2) {
		// Error - position out of bounds
		return;
	}

	lcdCursorPosition = line * LCD_SECOND_LINE_START_ADDR + position;
	lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR_REV |
			lcdReverseBits(lcdCursorPosition));
}


void lcdLine1() {
	lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR_REV |
			LCD_FIRST_LINE_START_ADDR_REV);
	lcdCursorPosition = LCD_FIRST_LINE_START_ADDR;
}


void lcdLine2() {
	lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR_REV |
			LCD_SECOND_LINE_START_ADDR_REV);
	lcdCursorPosition = LCD_SECOND_LINE_START_ADDR;
}


void lcdShiftCursorRight() {
	lcdWriteInstrRev(LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR_REV |
			LCD_SHIFT_RIGHT_REV);
	lcdCursorPosition++;
}


void lcdShiftCursorLeft() {
	lcdWriteInstrRev(LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR_REV);
	lcdCursorPosition--;
}


void lcdShiftDisplayRight() {
	lcdWriteInstrRev(LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR_REV |
			LCD_SHIFT_DISPLAY_REV | LCD_SHIFT_RIGHT_REV);
}


void lcdShiftDisplayLeft() {
	lcdWriteInstrRev(LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR_REV |
			LCD_SHIFT_DISPLAY_REV);
}


void lcdSaveSymbol(const uint8_t symCode, const uint8_t symData[]) {
	// Calculate actual address of symbol in CGRAM
	uint8_t addr = symCode * 8;

	lcdWriteInstrRev(LCD_SET_CGRAM_ADDR_INSTR_REV | lcdReverseBits(addr));

	for (uint8_t i = 0; i < 8;) {
		lcdWriteData(symData[i++]);
	}
}


void lcdCursorOn() {
	lcdState |= LCD_CURSOR_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdCursorOff() {
	lcdState &= ~LCD_CURSOR_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdBlinkerOn() {
	lcdState |= LCD_BLINKER_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdBlinkerOff() {
	lcdState &= ~LCD_BLINKER_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdDisplayOn() {
	lcdState |= LCD_DISPLAY_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdDisplayOff() {
	lcdState &= ~LCD_DISPLAY_ON_REV;
	lcdWriteInstrRev(lcdState);
}


void lcdClear() {
	lcdWriteInstrRev(LCD_CLEAR_DISPLAY_REV);
	lcdCursorPosition = 0;
}
