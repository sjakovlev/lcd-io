### What is LCD IO library? ###

This LCD IO library provides a simple interface to use ST7066U display module
in combination with custom AT90USB647 development board created by
University of Tartu and used in its "Microprocessors" course.

### Build dependencies ###

* avr-gcc
* avr-binutils
* avr-libc

### Optional dependencies ###

* avrdude - needed to flash built sample project to MCU
* doxygen - needed to build library documentation

### Building ###

To use this library in your project, add *include* folder to include path of
your project and compile library sources together with your project.
See *Makefile* for sample build parameters.

To build library object files, you can use `make all` command.

To build sample project, use one of the following commands:

* `make build/lcd_tester_linked.hex` to build sample project image;
* `make flash_lcd_tester` to build sample project image and flash it to MCU.

Note that for the second command to work, *avrdude* must be installed,
development board must also be connected and ready to be flashed.

To build documentation, use `make docs` command. Note that *doxygen* must be
installed for this command to succeed.

### Usage example ###

    #include <avr/io.h>
    #include "lcd_io.h"
    
    int main() {
    	lcdInit();
    	lcdCursorOff();
    	lcdBlinkerOff();
    	
    	lcdLine1();
    	lcdWriteSz("Hello");
    	lcdCursorToPosition(1, 5);
    	lcdWriteSz("World!");
    }

### License ###

The MIT License (MIT)

Copyright (c) 2015 Sergei Jakovlev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
